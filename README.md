# Quick Fix to Yahoo Mail Is Not Working on an Android Device [Case Study] #

There can be a number of reasons why your Yahoo Mail is not working properly on an Android device, such as you have not clear the data and cache for a long time.

But by following some general troubleshooting steps, you will be able to eradicate the occurring issue with your Yahoo mail on your Android smartphone.

You can access your Yahoo email account on your cell phone via two methods, either using a web browser or the Yahoo email application. Continue reading to know how you can resolve Yahoo mail related issues in no time.

## If you are using a Mobile browser ##

Many times, when you use the Yahoo mail mobile application, it crashes, freezes or runs very slow. Thus, preventing you to access your Yahoo mail account on an Android device.
To eliminate such glitches, follow the given troubleshooting steps:

### 1.Update the operating system of your Android device to the latest version. ###
When you run the Yahoo Mail application on an older operating system, the application becomes slow. Therefore, update it time to time.

### 2.Clear the cache and data of the Yahoo mail app ###
If you are not able to load your emails or attachments, it can be due to cache or temporary files that get collected on your Android devices over time. By deleting the temporary files and clearing up the cache, you can refresh the app without worrying about losing your data.

To clear the app cache, follow:

* At first, tap on the menu icon.
* Select the 'Settings' option.
* Tap on Clear cache option and once again, for confirmation.

To clear the application data, follow:

* At first, you have to open Android Settings app.
* Swipe down to the 'Device' and select the 'Applications' option.
* Tap on 'Application Manager'.
* In here, you will see Yahoo mail, tap on it.
* Select the 'Clear data' option.
* At the end, tap on 'Clear' for confirmation.

### 3.Force stop the Yahoo application and restart it again ###

If your application is running slow or freezing time to time, force-stop it. When you will restart it, the Yahoo Mail app will work better with the refreshed active memory. Follow:

* At first, tap on the Home button.
* Go to the Menu option and select the Settings.
* Tap on Application Manager.
* Find the Yahoo application and tap on it.
* There is a force stop option, click on it.
* Now, relaunch the application and check whether your issue is resolved or not.

### 4.Uninstall the Yahoo Mail app and reinstall it ###

For the uninstallation of Yahoo Mail application, follow:

* At first, go to the Home screen of your device and select the 'Settings' option.
* Under the 'Phone' section, tap on 'Apps'.
* Tap on 'Application Manager' now.
* From the list of options, select the 'Yahoo Mail' app.
* Tap on 'Uninstall' and for confirmation, select 'OK'.

After removing it successfully, you have to install it again. For installation of Yahoo app on an Android device, follow:

* Go to the Google Play Store.
* Enter the name of the application, i.e., Yahoo Mail in the given search box.
* Now, select the 'Install' option and an 'App permission' dialogue box will appear.
* Select the 'Accept' icon and your app will begin to download now.

When you use the Yahoo Mail app, some of its features are not available on your smartphone as they are only accessible from a desktop browser version. Some of them are mentioned below:

* Create vacation responses.
* View full email headers.
* Block email addresses.
* Send to a contact list.

## If you are using a Mobile browser ##

Issues with your mobile browser can prevent you to access the Yahoo pages on your Android phone. Follow the given steps to know how you to get back the access to Yahoo quickly.

Note: Make sure you are using the latest version of the mobile browser application. If not, update it first.

### Ensure your Android phone has access to the Internet network. Thus, check your Internet connection. ###
Force stop the mobile browser app and restart it. When you force-stop any application, its active memory refreshes.

### Clear the cache and cookies of the browser ###
Delete all the temporary files of your mobile browser. This will refresh them whenever you will launch the application next time.

### Turn the private browsing off ###
If you are accessing the Yahoo Mail on a private browser of your mobile, this will disable the cookies that are essential to provide you full Yahoo experience.

### Turn the location services on ###
To access some of the Yahoo features, you need to enable the location services to work in a proper manner.

### Reboot your device ###
Restart your phone as this will refresh your device memory

### Uninstall the browser app and reinstall it ###
There are chances the application is corrupted after the installation of a new copy of the application, your issue will be resolved.


If the problem still persists and you are not able to access your Yahoo Mail account either from a mobile browser or from its application, take further assistance from the yahoo customer support helpline number.


### Visit: 800support.net/yahoo-customer-service ###